#!/bin/zsh

cd "$(dirname "$0")"
SCRIPT_DIR=`pwd`

ITMSF="${SCRIPT_DIR}/itms_list.txt"
SURLF="${SCRIPT_DIR}/short_url.txt"

cp -f /dev/null "${SURLF}"
cp -f /dev/null "${ITMSF}"

find "." -name "*.plist" -type f -depth 3 | while read line; do
	URL="https://gitlab.com/sma11case/ipastore/raw/master/${line:2}"
	itms="itms-services://?action=download-manifest&url=${URL}"

	SURL=`"${HOME}/.oh-my-zsh/scripts/makeWBShareURL.sh" "${itms}"`
	
	echo "${line:2}" >> "${ITMSF}"
	echo "${itms}" >> "${ITMSF}"
	echo >> "${ITMSF}"
	
	echo "${line:2}" >> "${SURLF}"
	echo "${SURL}" >> "${SURLF}"
	echo >> "${SURLF}"
done

exit

