#!/bin/bash

SCRIPT_DIR=`dirname "$0"`
if [ '.' = "${SCRIPT_DIR:0:1}" ]; then
    SCRIPT_DIR="$(pwd)/${SCRIPT_DIR}"
fi
echo "SCRIPT_DIR = ${SCRIPT_DIR}"
cd "${SCRIPT_DIR}"

git add -A "*"
git add -A "itms-services/*"

COMMIT_MESSAGE='IPA Store by sma11case'
if [ -n "$1" ]; then
	COMMIT_MESSAGE=$1
fi
git commit -am "${COMMIT_MESSAGE}"

git push --all origin
git push --tags origin

exit 0

URL="itms-services://?action=download-manifest&url=https://coding.net/u/smallcase/p/ipastore/git/raw/master/itms-services/com.tencent.live4iphone.plist"
